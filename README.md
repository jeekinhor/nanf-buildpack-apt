# nanf-buildpack-apt

THIS BUILDPACK IS ORIGINALLY FORKED FROM HEROKU-BUILDPACK-APT @ https://github.com/heroku/heroku-buildpack-apt !
The author modified this buildpack for the Nanfuscator project to work with mingw and g++-multilib.
Additional compiler paths are configured in this script, namely CPATH variable.

Add support for apt-based dependencies during both compile and runtime.

## Usage

This buildpack is not meant to be used on its own, and instead should be in used in combination with Heroku's [multiple buildpack support](https://devcenter.heroku.com/articles/using-multiple-buildpacks-for-an-app).

Include a list of apt package names to be installed in a file named `Aptfile`

## Example

#### Command-line

```
heroku buildpacks:add --index 1 https://jeekinhor@bitbucket.org/jeekinhor/nanf-buildpack-apt.git
```

#### Aptfile

    g++-multilib

## License

MIT
